# Notes for configure Qt + OSG + OSGEarth

Linux下从零开始的 Qt + OSG + OSGEarth 配置，每一步都清晰明了，直到跑出一个地球。

争取将步骤和依赖库做到最精简，以方便移植。

- [Notes for configure Qt + OSG + OSGEarth](#notes-for-configure-qt--osg--osgearth)
  - [1. 安装 Qt](#1-安装-qt)
    - [1.1 下载安装包](#11-下载安装包)
    - [1.2 安装](#12-安装)
  - [2. 安装 OSG](#2-安装-osg)
    - [2.1 克隆源码](#21-克隆源码)
    - [2.2 配置模式](#22-配置模式)
    - [2.3 安装依赖](#23-安装依赖)
    - [2.4 编译安装](#24-编译安装)
    - [2.5 测试](#25-测试)
  - [3. 安装 OSGEarth](#3-安装-osgearth)
    - [3.1 克隆源码](#31-克隆源码)
    - [3.2 编译安装](#32-编译安装)
    - [3.3 测试](#33-测试)
  - [4. 运行Demo](#4-运行demo)

## 1. 安装 Qt

### 1.1 下载安装包

直接下载网络安装包，装最新版的。

国内镜像网站：`https://mirrors.nju.edu.cn/qt/official_releases/online_installers/`

### 1.2 安装

使用镜像加速：`./文件名 --mirror https://mirror.nju.edu.cn/qt`


## 2. 安装 OSG

接下来的 OSGEarth 的安装需要设置 OpenSceneGraph 目录内 CMakeLists.txt 文件中 OpenGL 的模式，所以不能为了图省事直接安装，而是要采用源码编译的方式进行安装。

### 2.1 克隆源码

`git clone -c http.sslVerify=false --recurse-submodules https://hub.njuu.cf/openscenegraph/OpenSceneGraph`

因为镜像站有时没有验证所以使用 `-c http.sslVerify=false` 跳过ssl验证，`--recurse-submodules` 递归克隆，使用加速镜像站。

2023.08.29 实测可用GitHub镜像站：https://hub.njuu.cf/

### 2.2 配置模式

在 OpenSceneGraph 目录下打开 CMakeLists.txt 文件，搜索关键词 `OPENGL_PROFILE` ，找到

```cmake
SET(OPENGL_PROFILE "GL2" CACHE STRING "OpenGL Profile to use, choose from GL1, GL2, GL3, GLES1, GLES2, GLES3")
```

这一行代码，Profile可能为 `GL2` 或其他选项，但是在这里一律将Profile修改为 `GL3` 。

### 2.3 安装依赖

Ubuntu直接执行 `sudo apt-get build-dep openscenegraph`

麒麟系统由于缺少 deb-src 源，无法执行上述命令，只能尝试一个一个手动寻找编译安装依赖组件或从Ubuntu上拷贝，很麻烦……

### 2.4 编译安装

```bash
cd OpenSceneGraph
mkdir build
cd build
cmake ..
make -j4
sudo cmake --install . --prefix /usr
```

### 2.5 测试

2023.08.29 Ubuntu 22.04.2 实测成功

```
$ osgversion 
OpenSceneGraph Library 3.7.0
```

如果找不到依赖库，就创建软连接 `ln -s` 或者直接 `cp` 对应库文件到相应目录下。


## 3. 安装 OSGEarth

### 3.1 克隆源码

`git clone -c http.sslVerify=false --recurse-submodules https://hub.njuu.cf/gwaldron/osgearth.git`

### 3.2 编译安装

```bash
cd osgearth
mkdir build
cd build
cmake ..
make -j4
sudo cmake --install . --prefix /usr
```

### 3.3 测试

2023.08.29 Ubuntu 22.04.2 实测成功

```
$ osgearth_version 
osgEarth 3.4.0 build 148
```

如果找不到依赖库，就创建软连接 ln -s 或者直接 cp 对应库文件到相应目录下。


## 4. 运行Demo

2023.08.29 Ubuntu 22.04.2

<img src="./wow.jpg" height="200px" />

---

2023.08.29
CQY @ TX
